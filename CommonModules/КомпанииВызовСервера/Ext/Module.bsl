﻿Функция ПолучитьОписаниеКомпаний(Параметры, Фильтры) Экспорт
	
	ОписаниеДанных = Новый Структура;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ ПЕРВЫЕ 1000
	               |	Компании.Ссылка КАК id,
	               |	Компании.Код КАК number,
	               |	Компании.Наименование КАК name,
	               |	Компании.ДатаРегистрации КАК registration_date,
	               |	Компании.Стоимость КАК price,
	               |	Компании.РасчетныйСчет.Наименование КАК checking_account_name,
	               |	Компании.Регион.Наименование КАК region_name,
	               |	Компании.ФормаОП.Наименование КАК legal_form_name,
	               |	КОЛИЧЕСТВО(Компании1.Ссылка) КАК Номер
	               |ПОМЕСТИТЬ ВтКомпании
	               |ИЗ
	               |	Справочник.Компании КАК Компании
	               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Компании КАК Компании1
	               |		ПО Компании.Наименование >= Компании1.Наименование
	               |
	               |СГРУППИРОВАТЬ ПО
	               |	Компании.Ссылка,
	               |	Компании.Код,
	               |	Компании.Наименование,
	               |	Компании.ДатаРегистрации,
	               |	Компании.Стоимость,
	               |	Компании.РасчетныйСчет.Наименование,
	               |	Компании.Регион.Наименование,
	               |	Компании.ФормаОП.Наименование
	               |
	               |УПОРЯДОЧИТЬ ПО
	               |	Компании.Наименование
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	КОЛИЧЕСТВО(*) КАК Количество
	               |ПОМЕСТИТЬ ВтКоличествоКомпаний
	               |ИЗ
	               |	Справочник.Компании КАК Компании
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ВтКомпании.id КАК id,
	               |	ВтКомпании.number КАК number,
	               |	ВтКомпании.name КАК name,
	               |	ВтКомпании.registration_date КАК registration_date,
	               |	ВтКомпании.price КАК price,
	               |	ВтКомпании.checking_account_name КАК checking_account_name,
	               |	ВтКомпании.region_name КАК region_name,
	               |	ВтКомпании.legal_form_name КАК legal_form_name,
	               |	ВтКоличествоКомпаний.Количество КАК Количество
	               |ИЗ
	               |	ВтКомпании КАК ВтКомпании,
	               |	ВтКоличествоКомпаний КАК ВтКоличествоКомпаний
	               |ГДЕ
	               |	ВтКомпании.Номер >= &НомерНачало
	               |	И ВтКомпании.Номер <= &НомерОкончание";
	
	Если ЗначениеЗаполнено(Параметры) И Параметры.Свойство("start") И Параметры.Свойство("end") Тогда 	
		НомерНачало = Параметры.start;
		НомерОкончание = Параметры.end;		
	Иначе
		НомерНачало = 1;
		НомерОкончание = 20;	
	КонецЕсли;	
	
	Запрос.УстановитьПараметр("НомерНачало", НомерНачало);
	Запрос.УстановитьПараметр("НомерОкончание", НомерОкончание);
		
	Записи = Запрос.Выполнить().Выгрузить();
	
	КоличествоКомпаний = Записи[0].Количество;
	
	Если ЗначениеЗаполнено(Фильтры) Тогда 
		ПрименитьФильтрКомпаний(Записи, Фильтры);	
	КонецЕсли;
	
	ОписаниеДанных.Вставить("amount", КоличествоКомпаний);
	ОписаниеДанных.Вставить("start", НомерНачало);
	ОписаниеДанных.Вставить("end", НомерОкончание);
	ОписаниеДанных.Вставить("company", ОбщегоНазначенияВызовСервера.ПреобразоватьТаблицуЗначенийВСтруктуру(Записи));
		
	Возврат ОписаниеДанных;
	
КонецФункции


Функция ОписаниеИменРеквизитов() 
	
	ОписаниеРеквизитов = Новый Структура;
	
	Компании = Новый Структура;
	Компании.Вставить("СправочникСсылка.Компании", "company");
	Компании.Вставить("Наименование", "name");
	Компании.Вставить("Ссылка", "id");
	Компании.Вставить("Код", "number");	
	Компании.Вставить("ДатаРегистрации", "registration_date");
	Компании.Вставить("Стоимость", "price");	
	ОписаниеРеквизитов.Вставить("СправочникСсылка.Компании", Компании);
	
	Банки = Новый Структура;
	Банки.Вставить("СправочникСсылка.Банки", "bank");
	Банки.Вставить("Наименование", "name");
	Банки.Вставить("Ссылка", "id");
	Банки.Вставить("Код", "number");	
	ОписаниеРеквизитов.Вставить("СправочникСсылка.Банки", Банки);
	
	РасчетныеСчета = Новый Структура;
	РасчетныеСчета.Вставить("СправочникСсылка.РасчетныйСчет", "checking_account");
	РасчетныеСчета.Вставить("Наименование", "name");
	РасчетныеСчета.Вставить("Ссылка", "id");
	РасчетныеСчета.Вставить("Код", "number");
	ОписаниеРеквизитов.Вставить("СправочникСсылка.РасчетныеСчета", РасчетныеСчета);
	
	Регионы = Новый Структура;
	Регионы.Вставить("СправочникСсылка.Регионы", "region");
	Регионы.Вставить("Наименование", "name");
	Регионы.Вставить("Ссылка", "id");
	Регионы.Вставить("Код", "number");
	Регионы.Вставить("КодРегиона", "code");
	ОписаниеРеквизитов.Вставить("СправочникСсылка.Регионы", Регионы);
	
	ФормыОП = Новый Структура;
	ФормыОП.Вставить("СправочникСсылка.ФормыОП", "legal_form");
	ФормыОП.Вставить("Наименование", "name");
	ФормыОП.Вставить("Наименование", "full_name");
	ФормыОП.Вставить("Ссылка", "id");
	ФормыОП.Вставить("Код", "number");	
	ОписаниеРеквизитов.Вставить("СправочникСсылка.ФормыОП", ФормыОП);
	
	Возврат ОписаниеРеквизитов;
	
КонецФункции

Процедура ПрименитьФильтрКомпаний(Компании, Фильтры)
	
КонецПроцедуры	








